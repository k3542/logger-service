package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/k3542/logger-service/consumer"
	"gitlab.com/k3542/logger-service/publisher"
)

func main() {
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:  "subscriber",
				Usage: "Service Subscriber",
				Action: func(c *cli.Context) error {
					consumer.SubscribeToLogger()
					return nil
				},
			},
			{
				Name:  "publisher",
				Usage: "Service Publisher",
				Action: func(c *cli.Context) error {
					publisher.PublishToLogger()
					return nil
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
