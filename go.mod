module gitlab.com/k3542/logger-service

go 1.15

require (
	github.com/rabbitmq/amqp091-go v1.3.4
	github.com/urfave/cli/v2 v2.8.1
)
